const debug = {};
debug.log = require('debug')('basics:bcrypt:general')
debug.read = require('debug')('basics:bcrypt:read');
debug.write = require('debug')('basics:bcrypt:write');
const bcrypt = require('bcrypt');

const Readable = require('stream').Readable;
const readable = new Readable();

let i = 0;

// readable._read = (n) => {
//     if (i++ < 10) {
//         debug.read('waiting for data...');
//         setTimeout(() => {
//             const data = {hash: 'abcd'};
//             debug.read(data.hash);
//             readable.push(JSON.stringify(data));
//         }, 2000);
//         // hash('abcd', 15).then(data => {
//         //     debug.read(data.hash);
//         //     readable.push(JSON.stringify(data));
//         // });
//     } else {
//         readable.push(null);
//     }
// }

const wait = [1,4,5,2,1];

readable._read = (n) => {
    // debug.read('read called');
}

const Writable = require('stream').Writable;
const writable = new Writable();

writable._write = (data, enc, next) => {
    // debug.write(JSON.parse(data).hash);
    debug.write(data.toString());
    next();
}

readable.pipe(writable);

async function hash(data, salt) {
    return {
        str: data,
        hash: await bcrypt.hash(data, salt)
    };
}

// setTimeout(() => {
//     while (i < wait.length) {
//         debug.read(`${i} - ${wait[i]}`);
//         setTimeout(((t) => {
//             debug.read(`finished waiting ${t}`);
//             readable.push(`${t}`);
//         }).bind(this, wait[i]), wait[i]*1000);
//         i++;
//     }
// }, 2000);

// const globalData = 'abcddffd';
//
// while (i++ < 5) {
//     const data = globalData + i;
//     debug.log(`started - ${i} - ${data}`);
//     ((j) => {
//         // hashing doesn't complete in order it was ran.
//         // maybe because of the CPU available for each process
//         // (some may win contention over the other even it it started late)
//         hash(data, 15)
//             .then(data => {
//                 debug.read(`read - ${j}`);
//                 data = JSON.stringify(data);
//                 debug.read(data);
//                 readable.push(data);
//             })
//             .catch(err => {
//                 debug.read(err);
//             });
//     }).bind(this, i)();
// }


