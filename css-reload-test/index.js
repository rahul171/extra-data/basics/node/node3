const path = require('path');
const debug = require('debug')('basics:extends');
const express = require('express');
const app = express();

const publicPath = path.join(__dirname, 'public');

app.use((req, res, next) => {
    debug(`${req.method} ${req.url}`);
    next();
});

app.use('/static', express.static(publicPath));

app.get('/', (req, res, next) => {
    res.render('index');
});

app.listen(3000, () => {
    debug('listening...');
});
