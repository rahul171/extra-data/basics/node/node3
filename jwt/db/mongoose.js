const mongoose = require('mongoose');

const connectUri = 'mongodb://127.0.0.1:27017/basics';

const connect = () => {
    return mongoose.connect(connectUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });
}

module.exports = { connect };
