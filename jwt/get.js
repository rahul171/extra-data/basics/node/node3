const connection = require('./db/mongoose');
const debug = require('debug')('basics:jwt:get');
const debug_ = require('debug')('basics:jwt:get_');

(async () => {

    await connection.connect();

    const User = require('./models/user');

    // const users = (await User.find({}).sort({ _id: -1 }).limit(2)).reverse();
    const users = await User.find({});

    debug(users);

})().catch(err => {
    debug_(err);
})
