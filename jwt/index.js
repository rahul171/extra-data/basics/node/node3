const debug = require('debug')('basics:jwt');
const debug_ = require('debug')('basics:jwt_');
const connection = require('./db/mongoose');
const jwt = require('jsonwebtoken');

const privateKey = 'abcd';

const token = jwt.sign({ id: this._id }, privateKey, { expiresIn: 60 });
debug(`token: ${token}`);
debug(jwt.verify(token, privateKey));

(async () => {
    // return;

    await connection.connect();
    debug('connected to the database');

    const User = require('./models/user');

    const random = Math.round(Math.random() * 1000);

    const user = new User({
        name: `rahul-${random}`,
        password: 'abdfjd',
        other: 'jdfjd'
    });

    // const user = await User.findOne({ name: 'rahul-793' });
    //
    // user.other = 'hello there';

    const res = await user.save();

    debug('saved');
    debug(res);

    const count = await User.countDocuments({});

    debug(`count: ${count}`);

})().catch(err => {
    debug_(err);
});
