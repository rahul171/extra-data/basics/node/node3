const mongoose = require('mongoose');
const schema = require('./schemas/user');

const Model =  mongoose.model('user', schema);

module.exports = Model;
