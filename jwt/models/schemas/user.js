const { Schema } = require('mongoose');
const bcrypt = require('bcrypt');

const schema = new Schema({
    name: {
        type: String,
        require: true,
        unique: true,
        minlength: 3,
        trim: true
    },
    password: {
        type: String,
        require: true,
        minlength: 3
    }
});

schema.methods.createHash = async function() {
    return await bcrypt.hash(this.password, 8);
}

schema.pre('save', async function(next) {
    console.log('before');
    console.log(this.isModified('password'));
    if (this.isModified('password')) {
        this.password = await this.createHash();
    } else {
        console.log('not modified');
    }
    console.log(this);
    console.log('-----');
    next();
})

schema.methods.generateAuthToken = async function() {}

module.exports = schema;
