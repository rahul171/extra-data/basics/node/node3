const debug = require('debug')('basics:mongoose');
const debug_ = require('debug')('basics:mongoose_');
const utils = require('@rahul171/utils');
const connection = require('./db/mongoose');
const User = require('./model/users');

connection.connect()
    .catch(err => {
        debug_(err);
        throw err;
    })
    .then(res => {

        // User.getUsers()
        //     .then(users => {
        //         debug(users);
        //     })
        //     .catch(err => {
        //         debug_(err);
        //     });

        User.insert({
            name: 'hello',
            email: 'someone@something.com',
            hey: 'there'
        })
            .then(res => {
                debug(res);
                return User.getUsers();
            })
            .then(users => {
                debug(users);
                debug(utils.getLine());
                // return User.Model.findByIdAndDelete(users[users.length - 1], { select: ['_id', 'name'] });
                // return User.Model.findOneAndDelete({ _id: users[users.length - 1]._id });
                return User.Model.findByIdAndUpdate(users[users.length - 1].id, {
                    name: 'fd'
                }, {
                    new: true,
                    runValidators: true
                    // lean: true
                });
                // return User.Model.findOneAndReplace({
                //     _id: users[users.length - 1]._id
                // }, {
                //     name: 'abcd'
                // }, {
                //     // new: true
                // });
            })
            .catch(({ message }) => {
                debug_(message);
            })
            .then(res => {
                debug(res);
                // console.log(res);
            });

    });
