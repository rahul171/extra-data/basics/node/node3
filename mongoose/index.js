const debug = require('debug')('basics:mongoose');
const debug_ = require('debug')('basics:mongoose_');
const validator = require('validator');
const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/basics', {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    debug('connected to the database');
}).catch(err => {
    debug_('error connecting to the database');
});

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
        validate(value) {
            if (value.includes('abcd')) {
                throw new Error('Name can\'t contain "abcd"');
            }
        }
    },
    email: {
        type: String,
        required: true,
        trim: true,
        validate(value) {
            console.log('here');
            if (!validator.isEmail(value)) {
                throw new Error('Invalid email bro');
            }
        }
    },
    data: {
        type: Array,
        required: false
    }
});

const User = mongoose.model('User', schema);

const user = new User({
    name: 'rah    ',
    email: '   hello@there.com  ',
    data: [1,2,()=>{}, function(){console.log('hello');}, {
        a: 1,
        b: 2
    },
    function() {
        const a = 1;
        console.log('a');
    }
    ]
});

const user1 = new User({
    name: 'rah    ',
    email: '   hello@there.com  ',
    data: undefined
});

// user1.save()
//     .then(res => {
//         debug(res);
//     })
//     .catch(err => {
//         debug('error');
//         debug_(err);
//     });

User.find({}).then(res => {
    debug(res);
}).catch(err => {
    debug_(err);
});
