const debug = require('debug')('basics:mongoose:db');
const debug_ = require('debug')('basics:mongoose:db_');
const mongoose = require('mongoose');

const connect = () => {
    return mongoose.connect('mongodb://127.0.0.1:27017/basics', {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false
    }).then((res) => {
        debug('connected to the database');
        return res;
    }).catch(err => {
        debug_('error connecting to the database');
        throw err;
    });
};

module.exports = { connect };
