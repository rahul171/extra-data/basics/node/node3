const { Schema } = require('mongoose');
const validator = require('validator');

module.exports = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        validate(value) {
            if (value.length > 20) {
                throw new Error('name is too long');
            }
        }
    },
    email: {
        type: String,
        required: false,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Invalid email');
            }
        }
    }
});
