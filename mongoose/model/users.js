const debug = require('debug')('basics:mongoose:model:users');
const debug_ = require('debug')('basics:mongoose:model:users_');
const mongoose = require('mongoose');
const schema = require('./schema/user');

const Model = mongoose.model('User', schema);

const getUsers = (condition = {}, returnParams) => {
    return Model.find(condition)
        .then((users) => {
            debug(`${users.length} users fetched`);
            return users;
        })
        .catch((err) => {
            debug_(err);
            throw err;
        });
};

const insert = (user) => {
    const userObj = new Model(user);
    return userObj.save()
        .then(res => {
            debug('saved');
            debug(res);
            return res;
        })
        .catch(err => {
            debug_(err);
            throw err;
        });
};

const create = (user) => {
    return Model.create(user)
        .then(res => {
            debug('created');
            debug(res);
            return res;
        })
        .catch(err => {
            debug_(err);
            throw err;
        });
}

module.exports = { Model, getUsers, insert, create };
