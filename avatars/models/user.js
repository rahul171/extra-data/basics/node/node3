const mongoose = require('mongoose');
const schema = require('./schemas/user');

const Model = mongoose.model('User', schema);

module.exports = Model;
