const mongoose = require('mongoose');
const schema = require('./schemas/avatar');

const Model = mongoose.model('Avatar', schema);

module.exports = Model;
