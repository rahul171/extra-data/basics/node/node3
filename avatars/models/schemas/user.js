const { Schema } = require('mongoose');

const schema = new Schema({
    name: {
        type: String,
        required: true,
        validate(value) {
            if (value.includes('abcd')) {
                throw new Error('abcd is not allowed');
            }
        }
    },
    other: {
        type: String
    }
});

schema.virtual('avatar', {
    ref: 'Avatar',
    localField: '_id',
    foreignField: 'userId'
});

module.exports = schema;
