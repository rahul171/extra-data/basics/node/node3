const { Schema } = require('mongoose');

const schema = new Schema({
    value: {
        type: Buffer,
        required: true
    },
    mimetype: {
        type: String,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectID,
        ref: 'User'
    }
});

module.exports = schema;
