const debug = require('debug')('basics:avatars');
const debug_ = require('debug')('basics:avatars_');

const connection = require('./db/mongoose');
const User = require('./models/user');
const Avatar = require('./models/avatar');

const express = require('express');
const app = express();

const multer = require('multer');

const sharp = require('sharp');

const upload = multer({
    // storage: multer.memoryStorage()
});

connection.connect()
    .then(() => {
        debug('connected');
    })
    .catch(err => {
        debug(err);
    });

app.get('/user/:id/avatar', async (req, res, next) => {

    const { value: avatar, mimetype: type } = await Avatar.findOne({
        userId: req.params.id
    }, {
        value: 1,
        mimetype: 1
    });

    res.set('Content-Type', type);
    
    res.status(200).send(avatar);
});

// app.get('/test', async (req, res, next) => {
//     throw new Error('some error');
// });
//
// app.use((err, req, res, next) => {
//     console.log(err);
// });

app.post('/user', upload.single('avatar'),
    async (req, res, next) => {

        if (!req.file) {
            return next(new Error('Avatar not provided'));
        }

        console.log(req.file);

        let buffer = req.file.buffer;

        // if (parseInt(req.query.fixedAvatar) === 1) {
        //     const a = buffer;
        //     const aa = new Uint8Array(a);
        //     const b = await sharp(buffer).toBuffer();
        //     const c = await sharp(buffer).png().toBuffer();
        //     // const b = await sharp(buffer).toBuffer();
        //     const d = await sharp(buffer).resize({ width: 250, height: 250 }).toBuffer();
        //     console.log(a);
        //     console.log(aa);
        //     console.log(b);
        //     console.log(c);
        //     console.log(a === b);
        //     console.log(b === c);
        //     console.log(d);
        //     debugger;
        //     return;
        // }

        if (parseInt(req.query.fixedAvatar) === 1) {
            buffer = await sharp(buffer).resize({ width: 250, height: 250 }).toBuffer();
            console.log(buffer.length);
        }

        const user = new User({ ...req.body });

        const avatar = new Avatar({
            value: buffer,
            mimetype: req.file.mimetype,
            userId: user._id
        });

        try {
            await user.save();
            await avatar.save();
        } catch(err) {
            return next(err);
        }

        res.status(200).send({ user });
    },
    (err, req, res, next) => {
        console.log('err 1');
        if (err instanceof multer.MulterError) {
            res.status(400).send({
                type: 'multer',
                message: err.message
            });
        } else {
            next(err);
        }
    },
    (err, req, res, next) => {
        console.log('err 2');
        res.status(400).send(err.message);
    });

app.listen(process.env.PORT, () => {
    debug(`listening on ${process.env.PORT}`);
})
