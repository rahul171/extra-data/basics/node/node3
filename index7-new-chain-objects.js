function Abcd() {
    this.a = { a: { a: 1 } };
}

Abcd.a = Abcd1;

function Abcd1() {
    this.a = { a: { a: 2 } };
}

Abcd1.a = Abcd2;

function Abcd2() {
    this.a = { a: { a: 3 } };
}

Abcd2.a = 4;

// change the () position to see the different results.
const a = new Abcd.a.a().a;

console.log(a);
