const fs = require('fs');
const noteFile = './files/notes.json';

function add({ title = '', body = '' } = {}) {
    const notes = getAll();
    notes.push({ title, body });
    return save(notes);
}

function save(notes) {
    return fs.writeFileSync(noteFile, JSON.stringify(notes));
}

function getAll() {
    return JSON.parse(fs.readFileSync(noteFile).toString());
}

module.exports = { add, get: getAll }
