const yargs = require('yargs');
const fs = require('fs/promises');
const note = require('./core');

const notesFile = ''

yargs.command({
    command: 'add',
    description: 'Add a note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler: (argv) => {
        note.add({
            title: argv.title,
            body: argv.body
        });
        console.log('Note added');
    }
});

yargs.command({
    command: 'read',
    description: 'Read notes',
    handler: (argv) => {
        console.log(note.get())
    }
});

yargs.parse();
