const express = require('express');
const app = express();
const multer = require('multer');
const path = require('path');
const debug = require('debug')('basics:upload');
const { line, getLine } = require('@rahul171/utils');

const upload = multer({
    // dest: path.join(__dirname, 'uploads'),
    storage: multer.diskStorage({
        destination: path.join(__dirname, 'uploads'),
        filename: (req, file, next) => {
            next(null, file.originalname);
        }
    }),
    fileFilter(req, file, next) {
        // console.log('inside filter');
        console.log(file);
        line();
        next(null, true);
    },
    limits: {
        // fields: 5,
        // non file field size
        fieldSize: 5,
        // including field name for files.
        fieldNameSize: 10,
        // fileSize: 2000 * 1024,
        // (no. of files) + fields
        // parts: 3
    }
});

app.post('/upload', upload.single('fileobj'), (req, res) => {
    res.send();
});

app.post('/upload1', upload.array('files', 5),
    (err, req, res, next) => {
        // console.log('inside err');
        // console.log(err instanceof multer.MulterError);
        // console.log(err);
        if (err instanceof multer.MulterError) {
            res.status(400).send(err.message);
            next(err);
        } else {
            next();
        }
    },
    (req, res) => {
        console.log(req.files.length);
        res.send();
    });

const upload2 = upload.fields([
    {
        name: 'files1',
        maxCount: 2
    },
    {
        name: 'files2',
        maxCount: 5
    }
]);

app.post('/upload2', upload2, (req, res) => {
    res.send();
});

app.listen(3000, () => {
    console.log('listening');
});
