const fs = require('fs');
const path = require('path');

const dir = path.join(__dirname, 'uploads');

const ext = 'png';

fs.readdir(dir, (err, files) => {
    for (const file of files) {
        if (file.endsWith(`.${ext}`)) {
            continue;
        }
        fs.rename(`${dir}/${file}`, `${dir}/${file}.${ext}`, () => {
            console.log('renamed');
        });
    }
});
