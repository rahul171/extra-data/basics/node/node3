const debug = require('debug')('basics:relationship');
const debug_ = require('debug')('basics:relationship_');
const connection = require('./db/mongoose');
const Thing = require('./models/thing');
const User = require('./models/user');

(async () => {

    await connection.connect();
    debug('connected');

    // await save();

    // debug((await User.findOne({ _id: '5ed29e59481ccb6cdf50f650' }).populate('thing')));
    // debug((await User.findOne({ _id: '5ed29e59481ccb6cdf50f650' }).populate('thing')));

    // not converted the output to 'Object' using toObject(), still toObject() is getting called
    // check schema file, virtuals were not being shown in the output but after setting virtuals to true
    // on toObject(), it shows, event if we are not using the toObject() here.
    console.log((await User.findOne({ _id: '5ed29e59481ccb6cdf50f650' }).populate('thing')));

    // debugger;

    // debug(await User.find({}));

})().catch(err => {
    debug_(err);
})

async function save() {
    const user = new User({
        name: 'rahul',
        other: 'hello there'
    });
    await user.save();
    debug('saved');
}
