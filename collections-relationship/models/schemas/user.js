const { Schema } = require('mongoose');

const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    other: {
        type: String
    }
});

schema.virtual('thing', {
    ref: 'Thing',
    localField: '_id',
    foreignField: 'owner'
});

schema.set('toJSON', { virtuals: true })
schema.set('toObject', { virtuals: true })

module.exports = schema;
