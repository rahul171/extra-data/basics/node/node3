const mongoose = require('mongoose');
const schema = require('./schemas/thing');

module.exports = mongoose.model('Thing', schema);
