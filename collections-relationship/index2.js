const debug = require('debug')('basics:relationship');
const debug_ = require('debug')('basics:relationship_');
const connection = require('./db/mongoose');
const Thing = require('./models/thing');
const User = require('./models/user');

(async () => {

    await connection.connect();
    debug('connected');

    // await save();

    // debug((await Thing.findById('5ed29e6b1f34176cf699a96a').populate('owner')).toObject());

    // debug(await Thing.find({}));
    debug(await Thing.find({}).populate('owner'));

})().catch(err => {
    debug_(err);
})

async function populate() {
    // const thing =
}

async function save() {
    const thing = new Thing({
        title: 'this is a thing',
        description: 'thing\'s description',
        owner: '5ed29e59481ccb6cdf50f650'
    });
    await thing.save();
    debug('saved');
}
