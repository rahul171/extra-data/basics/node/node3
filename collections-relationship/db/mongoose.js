const mongoose = require('mongoose');

const connect = async () => {
    return mongoose.connect('mongodb://127.0.0.1:27017/basics1', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });
}

module.exports = { connect };
