const a = 'e';

const b = Buffer.from(a);

console.log(a);
console.log(a.length);

console.log(b);
console.log(b.length);

// takes 2 bytes in utf-8.
const a1 = 'é';

const b1 = Buffer.from(a1);

console.log(a1);

// string length is always 1 though.
console.log(a1.length);

console.log(b1);

// 2 bytes.
console.log(b1.length);

debugger;
