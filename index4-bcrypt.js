const debug = require('debug')('basics:index4');
const debug_ = require('debug')('basics:index4_');
const bcrypt = require('bcrypt');

hash('abcde', 15, 3)
    .then((res) => {
        debug('---> done');
        (async () => {
            for (let i = 0; i < res.length; i++) {
                const item = res[i];
                debug(`${item.str} - ${item.hash}`);

                if (i === res.length - 2) {
                    // item.hash = item.hash.substring(0, item.hash.length - 2)
                    //     + item.hash.slice(-2).split('').reverse().join('');
                    const atIndex = 15;
                    const revLength = 2;
                    item.hash = item.hash.substring(0, atIndex)
                        + item.hash.slice(atIndex, atIndex + revLength).split('').reverse().join('')
                        + item.hash.substring(atIndex + revLength);
                }

                debug(`${item.str} - ${item.hash}`);
                const d = Date.now();
                debug(await bcrypt.compare(item.str, item.hash));
                // time diff is either 0ms or max time taken to create and compare the whole hash.
                // why?
                // maybe in order to prevent timing attacks
                // but bcrypt hash has different starting chars for 'abcd' and 'ancde',
                // so can't generate partial hash and compare.
                // so, it has to generate the whole hash to compare everytime,
                // but hashes are different even for the same string, then how does it compare?
                // after knowing how does it compare, will be able to dig further about why it takes 0 or max time,
                // but no in between.
                debug(`time diff for compare: ${Date.now() - d}ms`);
            }
        })();
    })
    .catch((err) => {
        debug_(err);
    })

async function hash(data, salt, n = 2) {
    const out = [];
    while (n-- > 0) {
        out.push({
            str: data,
            hash: await bcrypt.hash(data, salt)
        });
    }
    return out;
}
