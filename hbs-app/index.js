// NOTE: for some reason, the css auto reloads on the browser when changed here.

const path = require('path');
const debug = require('debug')('basics:extends');
const hbs = require('hbs');
const express = require('express');
const app = express();

const publicPath = path.join(__dirname, 'public')
const viewsPath = path.join(__dirname, 'views-hbs/templates');
const partialsPath = path.join(__dirname, 'views-hbs/partials');

app.set('view engine', 'hbs');
app.set('views', viewsPath);

hbs.registerPartials(partialsPath);

app.use((req, res, next) => {
    debug(`${req.method} ${req.url}`);
    next();
});

app.use('/static', express.static(publicPath));

app.get('/', (req, res, next) => {
    res.render('index', {
        title: 'Home',
        description: 'Hello there, this is home!'
    });
});

app.get('/data', (req, res, next) => {
    res.render('data', {
        title: 'Data',
        description: 'Hello there, this is data!'
    });
});

app.listen(process.env.NODE_PORT || 3000, () => {
    debug('listening...');
});
