const a = {
    a: 1,
    b: 2
};

a.toJSON = () => {
    return {a: 22};
}

const b = {
    a: 33,
    d: 2,
    f: () => {
        console.log('abcd');
    }
}

console.log(JSON.stringify({ a, b }));
