const debugParent = require('debug');
// const debug = debugParent('basics:mongo');
// const debug_ = debugParent('basics:mongo_');
const debug = debug_ = console.log;
const { MongoClient, ObjectID, ObjectId } = require('mongodb');

MongoClient.connect('mongodb://127.0.0.1:27017', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err, client) => {
    if (err) {
        debug_(err);
    }

    const db = client.db('a1');

    false && db.collection('aa1').insertOne({
        a1: 1,
        a2: 'hello',
        a3: function() {
            console.log('hello');
        },
        a4: [1,2,3,4, () => {}, {}]
    }, {}, (err, res) => {
        if (err) {
            debug_(err);
        }

        debug(res);
        debug(res.insertedId.id.toString());
        debug(res.insertedId.toString());
        debug(res.insertedId.toString().length);
        debug(res.insertedId.toHexString());
        debug(new ObjectID());

        res.insertedId.id.forEach(code => console.log(String.fromCharCode(code)));
    });

    const objId = new ObjectId();

    console.log(objId);
    console.log(objId.id);
    console.log(objId.toHexString());
    console.log(arrToHex(objId.id));

    objId.id.forEach(code => {
        console.log(String.fromCharCode(code));
    });
});

function arrToHex(arr) {
    let hex = '';
    arr.forEach((item) => {
        hex += toHex(item);
    });
    return hex;
}

function toHex(num) {
    let hex = [];
    const map = [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'];
    do {
        const mod = num % 16;
        hex.push(map[mod]);
        num = Math.floor(num / 16);
    } while(num >= 16);
    hex.push(map[num]);
    for (let i = 0, j = hex.length - 1; i < j; i++, j--) {
        const temp = hex[i];
        hex[i] = hex[j];
        hex[j] = temp;
    }
    return hex.join('');
}
