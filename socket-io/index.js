const socketio = require('socket.io');
const http = require('http');
const express = require('express');
const app = express();
const server = http.createServer(app);
const io = socketio(server);

io.on('connection', (socket) => {
    socket.emit('hello', 'hello there');
    socket.on('disconnect', () => {
        console.log('disconnected');
    });
    socket.on('data', (data) => {
        console.log(`data: ${data}`);
    });
    setInterval(() => {
        socket.emit('hello', 'fddfdff');
    }, 3000);
});

app.use('/static', express.static('public'));

app.get('/', (req, res) => {
    res.send('abcd');
})

server.listen(3000, () => {
    console.log('listening on 3000');
});
