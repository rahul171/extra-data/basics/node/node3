const debug = require('debug')('basics:index2');
const debug_ = require('debug')('basics:index2_');

const fn = (a, b) => {
    return new Promise((resolve, reject) => {
        debug('run');
        setTimeout(() => {
            if (a % 2 === 0) {
                resolve(a + b);
            }
            reject(b);
        }, 1000);
    });
}

fn(2, 1)
    .then(sum => {
        debug(`sum: ${sum}`);
        return new Promise((resolve,reject)=>{reject('hello there');});
    })
    .then(sum => {
        debug(`another: ${sum}`);
    })
    .catch(err => {
        debug_(`err: ${err}`);
        // return 'yes';
        throw new Error('fdfd');
        // return 'string';
    })
    .then(data => {
        debug(`data: ${data}`);
    })
    .catch(err => {
        debug_(`another err: ${err.message}`);
    })
    .then(data => {
        debug(`data 1: ${data}`);
    })
    .then(data => {
        debug(`data 2: ${data}`);
    })
    .then(data => {
        debug(`data 3: ${data}`);
    })
    .then(data => {
        debug(`data 4: ${data}`);
    })

