console.log('generating...');

const a = generateSeqBuffer(2000000, 10);
const b = generateSeqBuffer(2000000, 19900);

console.log('generated!');

const d = Date.now();
console.log(compare(a, b));
console.log(Date.now() - d);

const dd = Date.now();
console.log(compareConstantTime(a, b));
console.log(Date.now() - dd);

function toBinary(n) {
    const out = [];
    do {
        out.push(n % 2);
        n = Math.floor(n / 2);
    } while(n >= 2);
    out.push(n);
    for (let i = out.length; i < 8; i++) {
        out.push(0);
    }
    for (let i = 0, j = out.length - 1; i < j; i++, j--) {
        const temp = out[i];
        out[i] = out[j];
        out[j] = temp;
    }
    return out;
}

function compare(a, b) {
    if (a.length !== b.length) {
        return false;
    }

    for (let i = 0; i < a.length; i++) {
        const binA = toBinary(a[i]);
        const binB = toBinary(b[i]);

        let result = 0;

        for (let j = 0; j < binA.length; j++) {
            result |= binA[j] ^ binB[j];
        }

        if (result !== 0) {
            return false;
        }
    }

    return true;
}

function compareConstantTime(a, b) {
    if (a.length !== b.length) {
        return false;
    }

    let finalResule = 0;

    for (let i = 0; i < a.length; i++) {
        const binA = toBinary(a[i]);
        const binB = toBinary(b[i]);

        let result = 0;

        for (let j = 0; j < binA.length; j++) {
            result |= binA[j] ^ binB[j];
        }

        finalResule |= result;
    }

    return finalResule === 0;
}

function generateSeqBuffer(n, p) {
    const b = Buffer.alloc(n);
    let j = 0;
    const baseStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456780';
    const length = baseStr.length;
    let s = '';
    for (let i = 0; i < n; i++) {
        let val = baseStr[j];
        if (i === p) {
            val = '9';
        }
        s += val;
        j = ++j % length;
    }
    b.write(s);
    return b;
}
