const parent = {
    a: 11,
    b: [1,2,3,4],
    c: function() {
        console.log(this);
    },
    d: () => {
        console.log(this);
    },
    e: {
        a: function() {
            console.log(this);
        },
        b: () => {
            console.log(this);
        }
    },
    f: function() {},
    g: function() {
        (() => {
            console.log(this);
        })();
        (function(){
            console.log(this);
        })();
        console.log(this);
    },
    h: function() {}
};

parent.f.prototype.a = function() {
    console.log(this);
}

parent.f.prototype.b = () => {
    console.log(this);
}


this.abcd = 'hello';

console.log('parent.c()');
parent.c();
console.log('parent.d()');
parent.d();
console.log('parent.e.a()');
parent.e.a();
console.log('parent.e.b()');
parent.e.b();

const b = {
    a: function() {
        parent.e.b();
        console.log(this);
    }
}

console.log('b.a()');
b.a();

console.log('new parent.f().a()');
new parent.f().a();
console.log('new parent.f().b()');
new parent.f().b();

console.log('parent.g()');
parent.g();

parent.h.prototype.a = function() {

}

const c = {
    a: function() {
        function a() {
            console.log(this);
        }
        const abcd = () => {
            console.log(this);
        };
        a();
        abcd();
        console.log(this);
    },
    msg: 'hello'
}

const d = {
    a: function() {
        c.a();
    }
}

console.log('c.a()');
c.a();

console.log('d.a()');
d.a();

function A() {
    this.a = function() {
        console.log(this);
    };

    this.b = () => {
        console.log(this);
    }

    this.aa = function(fn) {
        fn();
    }

    this.a();
}

console.log('A');
const aa = new A();
aa.a();
aa.b();

aa.aa(function() {
    console.log(this);
});

aa.aa(() => {
    console.log(this);
});

const a1 = {
    a: function(fn) {
        fn();
    },
    b: (fn) => {
        fn();
    }
}

console.log('a1');

a1.a(function() {
    console.log(this);
});

a1.a(() => {
    console.log(this);
});

const a2 = {
    a: function(fn) {
        a1.a(fn);
    }
};

console.log('a2');
a2.a(function() {
    console.log(this);
});
a2.a(() => {
    console.log(this);
});

console.log(global.abcd);
console.log(this.abcd);

const a3 = {
    a: 1,
    b: this,
    c: this.d,
    d: 22
}

console.log('a3');
// console.log(a3.a);
console.log(a3.b);
console.log(a3.c);
// console.log(a3.d);

debugger;
